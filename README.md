# OpenML dataset: prnn_synth

https://www.openml.org/d/464

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: B.D. Ripley  
**Source**: Unknown - Date unknown  
**Please cite**:   

Dataset from `Pattern Recognition and Neural Networks' by B.D. Ripley. Cambridge University Press (1996)  ISBN  0-521-46086-7. The background to the datasets is described in section 1.4; this file relates the computer-readable files to that description.

**synthetic two-class problem**
Data from Ripley (1994a).

This has two real-valued co-ordinates (xs and ys) and a class (xc)
which is 0 or 1.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/464) of an [OpenML dataset](https://www.openml.org/d/464). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/464/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/464/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/464/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

